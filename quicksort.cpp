#include "quicksort.h"
#include "writetojson.h"
#include <algorithm>

void QuickSort::sort(std::vector<int> &arr)
{
    std::sort(arr.begin(), arr.end());
}

void QuickSort::accept(WriteToJson *p)
{
    p->accept(this);
}
