#ifndef BUBBLESORT_H
#define BUBBLESORT_H

#include "isort.h"

class BubbleSort : public ISort
{
public:
    void sort(std::vector<int>& arr) override;
    void accept(WriteToJson *) override;
};

#endif // BUBBLESORT_H
