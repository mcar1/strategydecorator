#ifndef WRITETOJSON_H
#define WRITETOJSON_H
#include <iostream>
//#include "bigclassforvector.h"
//#include "bubblesort.h"
//#include "logsort.h"
//#include "quicksort.h"
//#include "testtimesort.h"

class BigClassForVector;
class BubbleSort;
class LogSort;
class QuickSort;
class TestTimeSort;



class WriteToJson
{
public:
    void accept(BigClassForVector*);
    void accept(BubbleSort*);
    void accept(LogSort*);
    void accept(QuickSort*);
    void accept(TestTimeSort*);
    WriteToJson(std::ostream&);
private:
    void printTabs();
    int m_tabs=0;
    std::ostream& out;
};

#endif // WRITETOJSON_H
