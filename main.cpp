#include <iostream>
#include <fstream>
#include "bigclassforvector.h"
#include "quicksort.h"
#include "bubblesort.h"
#include "testtimesort.h"
#include "logsort.h"
#include "writetojson.h"

int main()
{
    auto a = BigClassForVector(std::make_unique<LogSort>(
                                   std::make_unique<TestTimeSort>(
                                       std::make_unique<QuickSort>()
                                       )
                                   )
                               );

//    auto a = BigClassForVector(std::make_unique<TestTimeSort>(
//                                   std::make_unique<LogSort>(
//                                       std::make_unique<BubbleSort>()
//                                       )
//                                   )
//                               );

    /*auto a = BigClassForVector(
                std::make_unique<TestTimeSort>(
                    std::make_unique<BubbleSort>()
                    )
                );*/
//    a.add(5);
//    a.add(3);
//    a.add(7);
//    a.add(-2);
//    a.add(18);
    std::srand(time(0));
    for(int i=0; i<20; i++)
    {
        a.add(rand()%201-100);
    }
    a.sort();

    std::ofstream outf("out.txt");

    WriteToJson json(outf);
    a.accept(&json);


    return 0;

}
