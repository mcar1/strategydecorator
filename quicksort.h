#ifndef QUICKSORT_H
#define QUICKSORT_H

#include "isort.h"

class QuickSort : public ISort
{
public:
    void sort(std::vector<int>& arr) override;
    void accept(WriteToJson *) override;
};

#endif // QUICKSORT_H
