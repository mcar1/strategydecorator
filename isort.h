#ifndef ISORT_H
#define ISORT_H
#include <vector>
class BigClassForVector;
class WriteToJson;
class ISort
{
public:
    virtual void sort(std::vector<int>&)=0;
    virtual void accept(WriteToJson*)=0;
};
#endif // ISORT_H
