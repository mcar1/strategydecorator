#include "writetojson.h"
#include "bigclassforvector.h"
#include "bubblesort.h"
#include "logsort.h"
#include "quicksort.h"
#include "testtimesort.h"

WriteToJson::WriteToJson(std::ostream& out):out(out) {}

void WriteToJson::printTabs()
{
    for(int i=0; i<m_tabs; i++){
        out<<"\t";
    }
}

void WriteToJson::accept(BigClassForVector* v)
{
    printTabs();
    out<<"{\n";

    m_tabs++;
    printTabs();
    out<<"\"vector\": [";
    for(int i=0; i<v->m_arr.size()-1; ++i)
    {
        out<<"\""<<v->m_arr[i]<<"\", ";
    }
    out<<"\""<<v->m_arr[v->m_arr.size()-1]<<"\"";
    out<<"],\n";

    printTabs();
    out<<"\"psort\":\n";

    v->m_psort->accept(this);

    m_tabs--;
    printTabs();
    out<<"}\n";
}

void WriteToJson::accept(BubbleSort *)
{
    printTabs();
    out<<"{\n";

    m_tabs++;
    printTabs();
    out<<"\"name\":\"bubblesort\"\n";


    m_tabs--;
    printTabs();
    out<<"}\n";
}

void WriteToJson::accept(LogSort* p)
{
    printTabs();
    out<<"{\n";

    m_tabs++;
    printTabs();
    out<<"\"name\":\"logsort\",\n";

    printTabs();
    out<<"\"psort\":\n";

    p->m_p->accept(this);

    m_tabs--;
    printTabs();
    out<<"}\n";
}

void WriteToJson::accept(QuickSort *)
{
    printTabs();
    out<<"{\n";

    m_tabs++;
    printTabs();
    out<<"\"name\":\"quicksort\"\n";


    m_tabs--;
    printTabs();
    out<<"}\n";
}

void WriteToJson::accept(TestTimeSort *p)
{
    printTabs();
    out<<"{\n";

    m_tabs++;
    printTabs();
    out<<"\"name\":\"testtimesort\",\n";

    printTabs();
    out<<"\"psort\":\n";

    p->m_p->accept(this);

    m_tabs--;
    printTabs();
    out<<"}\n";
}
