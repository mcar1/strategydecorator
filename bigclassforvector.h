#ifndef BIGCLASSFORVECTOR_H
#define BIGCLASSFORVECTOR_H

#include <vector>
#include <memory>
#include "isort.h"
//include "writetojson.h"
class WriteToJson;

class BigClassForVector
{
private:
    std::vector<int> m_arr;
    std::unique_ptr<ISort> m_psort;
public:
    BigClassForVector(std::unique_ptr<ISort>&&);
    void add(int);
    void sort();
    void print();
    void accept(WriteToJson*);
    friend class WriteToJson;
};

#endif // BIGCLASSFORVECTOR_H
