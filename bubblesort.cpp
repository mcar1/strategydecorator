#include "bubblesort.h"
#include "writetojson.h"


void BubbleSort::sort(std::vector<int> &arr)
{
    while(true)
    {
        bool f=false;
        for(int i=0; i<arr.size()-1; i++)
        {
            if(arr[i]>arr[i+1])
            {
                std::swap(arr[i],arr[i+1]);
                f=true;
            }
        }
        if(!f) break;
    }
}

void BubbleSort::accept(WriteToJson *p)
{
    p->accept(this);
}
