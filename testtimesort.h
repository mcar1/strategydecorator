#ifndef TESTTIMESORT_H
#define TESTTIMESORT_H

#include "isort.h"
#include <memory>


class TestTimeSort : public ISort
{
private:
    std::unique_ptr<ISort> m_p;
public:
    TestTimeSort(std::unique_ptr<ISort>&& p);
    void sort(std::vector<int>& arr) override;
    void accept(WriteToJson *) override;
    friend WriteToJson;
};
#endif // TESTTIMESORT_H
