#include "logsort.h"
#include "bigclassforvector.h"
#include "writetojson.h"
#include <iostream>
void LogSort::print(const std::vector<int> &arr) const
{
    for(auto i:arr)
    {
        std::cout<<i<<" ";
    }
    std::cout<<std::endl;
}

LogSort::LogSort(std::unique_ptr<ISort> &&p)
    :m_p(std::move(p))
{}

void LogSort::sort(std::vector<int> &arr)
{
    std::cout<<"presort:\t";
    print(arr);
    m_p->sort(arr);
    std::cout<<"\npostsort:\t";
    print(arr);
}

void LogSort::accept(WriteToJson *p)
{
    p->accept(this);
}
