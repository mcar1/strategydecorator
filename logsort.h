#ifndef LOGSORT_H
#define LOGSORT_H

#include "isort.h"
#include <memory>

class LogSort : public ISort
{
private:
    std::unique_ptr<ISort> m_p;
    void print(const std::vector<int>& arr) const;
public:
    LogSort(std::unique_ptr<ISort>&& p);
    void sort(std::vector<int>& arr) override;
    void accept(WriteToJson *) override;
    friend WriteToJson;
};

#endif // LOGSORT_H
