#include "testtimesort.h"
#include "writetojson.h"
#include <iostream>
#include <time.h>
#include <chrono>

TestTimeSort::TestTimeSort(std::unique_ptr<ISort> &&p)
    :m_p(std::move(p))
{}

void TestTimeSort::sort(std::vector<int> &arr)
{
    std::cout<<"start test time\n";
    auto start = std::chrono::steady_clock::now();
    m_p->sort(arr);
    auto end = std::chrono::steady_clock::now();
    auto elapsed_ms = std::chrono::duration_cast<std::chrono::microseconds>(end - start);
    std::cout<<"result time: "<<elapsed_ms.count()<<"us\n";
}

void TestTimeSort::accept(WriteToJson *p)
{
    p->accept(this);
}
