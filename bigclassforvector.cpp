#include "bigclassforvector.h"
#include "writetojson.h"
#include <iostream>
#include <algorithm>

BigClassForVector::BigClassForVector(std::unique_ptr<ISort> &&p)
    :m_psort(std::move(p))
{}
void BigClassForVector::add(int x)
{
    m_arr.push_back(x);
}

void BigClassForVector::sort()
{
    m_psort->sort(m_arr);
}

void BigClassForVector::print()
{

    for(auto i:m_arr)
    {
        std::cout<<i<<" ";
    }
    std::cout<<std::endl;

}

void BigClassForVector::accept(WriteToJson *p)
{
    p->accept(this);
}


